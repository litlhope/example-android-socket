package kr.heja.exam.drone;

import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = "Main";

	private static final int PORT = 9001;	// 소켓서버를 실행할 포트

	@BindView(R.id.tvLog) TextView tvLog;

	@BindView(R.id.tvThrottleUp) TextView tvThrottleUp;
	@BindView(R.id.tvThrottleDown) TextView tvThrottleDown;
	@BindView(R.id.tvTurnLeft) TextView tvTurnLeft;
	@BindView(R.id.tvTurnRight) TextView tvTurnRight;

	@BindView(R.id.tvGoForward) TextView tvGoForward;
	@BindView(R.id.tvGoBackward) TextView tvGoBackward;
	@BindView(R.id.tvGoLeft) TextView tvGoLeft;
	@BindView(R.id.tvGoRight) TextView tvGoRight;

	private StringBuffer mLog;
	private String mLocalIp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// TODO: Wi-Fi 상태 변경에 따라서 Wi-Fi 연결되면 자동으로 서버가 시작하도록 변경 필요.
		//       현재는 앱 시작시 서버시작하며, Wi-Fi가 꺼져있으면 서버가 시작하지 않음.

		init();
	}

	@Override
	protected void onStart() {
		super.onStart();
		// 소켓 서버 실행
		DroneControlServer server = new DroneControlServer();
		server.start();
	}

	@Override
	protected void onStop() {
		super.onStop();
		// 각 스레드 종료
		ReplySender.currentThread().interrupt();
		DroneControlReceiver.currentThread().interrupt();
		DroneControlServer.currentThread().interrupt();

		// 소켓 종료
		// TODO: 서버소켓만 종료처리하면 되는건지, 사용한 소켓 및 스트림까지 전부 close 처리 해야 하는지 검토 필요.
		if (mServerSocket != null) {
			try {
				mServerSocket.close();
			} catch (IOException e) {
				Log.w(TAG, "", e);
			}
		}
	}

	private void init() {
		ButterKnife.bind(this);
		mLog = new StringBuffer();

		// 로그 표시용 텍스트뷰에 스크롤 기능 추가.
		tvLog.setMovementMethod(new ScrollingMovementMethod());
	}

	/*
	 * Wi-Fi에 할당된 IP 주소를 조회한다.
	 */
	private void getLocalIp() {
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ipAddress = wifiInfo.getIpAddress();

		Log.d(TAG, "wifiAddress: " + ipAddress);
		if (ipAddress == 0) {
			// Wi-Fi가 꺼져있는 경우 IP주소로 0이 반환된다.
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(MainActivity.this, "Wi-Fi를 켜주세요.", Toast.LENGTH_SHORT).show();
				}
			});
			mLocalIp = null;
		} else {
			mLocalIp = String.format(Locale.getDefault(), "%d.%d.%d.%d",
					(ipAddress & 0xff),
					(ipAddress >> 8 & 0xff),
					(ipAddress >> 16 & 0xff),
					(ipAddress >> 24 & 0xff));
		}

		Log.d(TAG, "mLocalIp: " + mLocalIp);
	}

	private boolean execCommand(String cmd) {
		boolean isSuccess = false;

		Log.d(TAG, "execCommand[" + cmd + "]");
		// 커맨드에 해당하는 텍스트뷰를 가져온다.
		final TextView tv = cmd.startsWith("THROTTLE_UP") ? tvThrottleUp
				: cmd.startsWith("THROTTLE_DOWN") ? tvThrottleDown
				: cmd.startsWith("TURN_LEFT") ? tvTurnLeft
				: cmd.startsWith("TURN_RIGHT") ? tvTurnRight
				: cmd.startsWith("GO_FORWARD") ? tvGoForward
				: cmd.startsWith("GO_BACKWARD") ? tvGoBackward
				: cmd.startsWith("GO_LEFT") ? tvGoLeft
				: cmd.startsWith("GO_RIGHT") ? tvGoRight
				: null;

		if (tv != null) {
			if (cmd.endsWith("START")) {
				// 컨트롤러 버튼이 눌린경우(Touch down) 텍스트뷰의 배경색을 검정색으로 설정(글자색은 흰색)
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						tv.setBackgroundColor(Color.BLACK);
						tv.setTextColor(Color.WHITE);
					}
				});
				isSuccess = true;

			} else if (cmd.endsWith("STOP")) {
				// 컨트롤러 버튼이 떨어진경우(Touch up) 텍스튜뷰를 원복한다.(배경색 흰색, 글자색 검정)
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						tv.setBackgroundColor(Color.WHITE);
						tv.setTextColor(Color.BLACK);
					}
				});
				isSuccess = true;
			}
		}

		return isSuccess;
	}

	private void log(final String msg) {
		// 소켓처리용 스레드에서 호출 할 수 있도록 runOnUiThread에서 로그 기록하도록 한다.
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
				String dateStr = format.format(new Date(System.currentTimeMillis()));
				Log.d(TAG, dateStr + " : " + msg);

				// 최근 로그가 상단에 기록 되도록 한다.
				mLog.insert(0, dateStr + " : " + msg + "\n");

				tvLog.setText(mLog.toString());
			}
		});
	}

	/*
	 * 스레드 목록
	 * 1. DroneControlServer - [소켓서버 실행 스레드] 실행 후 리시버 스레드 실행
	 * 2. DroneControlReceiver - [리시버 스레드] 클라이언트로부터 커맨드 수신 후 응답 스레드 실행
	 * 3. ReplySender - [응답 스레드] 클라이언트에게 응답("OK" or "FAIL") 후 리시버 스레드 실행
	 *
	 * [리시버스레드]와 [응답스레드]간 재귀 호출로 응답대기상태를 계속 유지한다.
	 * 참고: while(true) 를 이용하여 무한루프로 구현했으나, 1회 커맨드 수신 후 루프가 되지 않았음
	 */
	private ServerSocket mServerSocket;
	private class DroneControlServer extends Thread {
		@Override
		public void run() {
			try {
				getLocalIp();
				if (mLocalIp == null) {
					log("로컬 IP 조회 실패! - Wi-Fi를 켠 후 다시 시도해 주세요.");
				} else {
					// 소켓서버 실행
					log("Listening... [" + mLocalIp + ":" + PORT + "]");
					mServerSocket = new ServerSocket(PORT);

					// 리시버 스레드 실행
					Log.d(TAG, "Start receiver!");
					Socket socket = mServerSocket.accept();
					DroneControlReceiver receiver = new DroneControlReceiver(socket);
					receiver.start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class DroneControlReceiver extends Thread {
		private Socket mSocket;
		DroneControlReceiver(Socket socket) {
			mSocket = socket;
		}

		@Override
		public void run() {
			try {
				// 커맨드 수신 대기
				log("Receiving...");
				BufferedReader in = new BufferedReader(
						new InputStreamReader(mSocket.getInputStream()));
				String line = in.readLine();
				log("Received - " + line);

				// 수신된 커맨드 처리
				boolean isSuccess = execCommand(line);

				// 클라이언트로 응답 처리
				ReplySender replySender = new ReplySender(mSocket,
						isSuccess ? "OK" : "FAIL");
				replySender.start();
			} catch (IOException ex) {
				Log.w(TAG, "", ex);
				log(ex.getMessage());
			}
		}
	}

	private class ReplySender extends Thread {
		private Socket mSocket;
		private String mReplyMsg;

		ReplySender(Socket socket, String replyMsg) {
			mSocket = socket;
			mReplyMsg = replyMsg;
		}

		@Override
		public void run() {
			PrintWriter out;
			try {
				// 클라이언트에 응답처리
				out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(mSocket.getOutputStream())));
				out.println(mReplyMsg);
				Log.d(TAG, "Reply completed!");

				// 리시버 스레드 실행 - 커맨드 명령 수신 대기
				DroneControlReceiver receiver = new DroneControlReceiver(mSocket);
				receiver.start();

			} catch (IOException ex) {
				Log.w(TAG, "", ex);
			}
		}
	}
}
