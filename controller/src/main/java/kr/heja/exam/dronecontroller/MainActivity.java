package kr.heja.exam.dronecontroller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
	private static final String TAG = "Main";

	@BindView(R.id.etHost) EditText etHost;
	@BindView(R.id.etPort) EditText etPort;

	@BindView(R.id.btnThrottleUp) Button btnThrottleUp;
	@BindView(R.id.btnThrottleDown) Button btnThrottleDown;
	@BindView(R.id.btnTurnLeft) Button btnTurnLeft;
	@BindView(R.id.btnTurnRight) Button btnTurnRight;
	@BindView(R.id.btnGoForward) Button btnGoForward;
	@BindView(R.id.btnGoBackward) Button btnGoBackward;
	@BindView(R.id.btnGoLeft) Button btnGoLeft;
	@BindView(R.id.btnGoRight) Button btnGoRight;

	private Socket mSocket;
	private BufferedWriter mSocketOut;
	private BufferedReader mSocketIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		init();
	}

	@Override
	protected void onStop() {
		super.onStop();

		// 소켓 및 스트림 close 처리
		if (mSocket != null && mSocket.isConnected()) {
			try {
				mSocket.close();
			} catch (IOException ex) {
				Log.w(TAG, "", ex);
			}
		}

		if (mSocketOut != null) {
			try {
				mSocketOut.close();
			} catch (IOException ex) {
				Log.w(TAG, "", ex);
			}
		}

		if (mSocketIn != null) {
			try {
				mSocketIn.close();
			} catch (IOException ex) {
				Log.w(TAG, "", ex);
			}
		}
	}

	private void init() {
		ButterKnife.bind(this);

		// 버튼이 눌렸을때와 떨어졌을때 각각 처리하기 위해서 터치 이벤트를 이용한다.
		btnThrottleUp.setOnTouchListener(this);
		btnThrottleDown.setOnTouchListener(this);
		btnTurnLeft.setOnTouchListener(this);
		btnTurnRight.setOnTouchListener(this);
		btnGoForward.setOnTouchListener(this);
		btnGoBackward.setOnTouchListener(this);
		btnGoLeft.setOnTouchListener(this);
		btnGoRight.setOnTouchListener(this);
	}

	// 소켓 서버(드론 시뮬레이터)에 커맨드 전송.
	private void sendCommand(String cmd) {
		Log.d(TAG, "cmd: " + cmd);
		DroneServerSender sender = new DroneServerSender(cmd);
		sender.start();
	}

	@OnClick(R.id.btnConnect)
	public void connectDroneServer(View view) {
		// 드론 시뮬레이터에 연결 시도.
		DroneServerConnector connector = new DroneServerConnector();
		connector.start();
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		String prefix = null;
		String postfix = null;

		// 전송할 커맨드를 생성한다. 커맨드는 "버튼이름_눌린상태" 이다.
		switch (view.getId()) {
			case R.id.btnThrottleUp:
				prefix = "THROTTLE_UP";
				break;
			case R.id.btnThrottleDown:
				prefix = "THROTTLE_DOWN";
				break;
			case R.id.btnTurnLeft:
				prefix = "TURN_LEFT";
				break;
			case R.id.btnTurnRight:
				prefix = "TURN_RIGHT";
				break;
			case R.id.btnGoForward:
				prefix = "GO_FORWARD";
				break;
			case R.id.btnGoBackward:
				prefix = "GO_BACKWARD";
				break;
			case R.id.btnGoLeft:
				prefix = "GO_LEFT";
				break;
			case R.id.btnGoRight:
				prefix = "GO_RIGHT";
				break;
		}

		switch (motionEvent.getAction()) {
			case MotionEvent.ACTION_DOWN:
				postfix = "START";
				break;
			case MotionEvent.ACTION_UP:
				postfix = "STOP";
				break;
		}

		if (prefix != null && postfix != null) {
			// DOWN과 UP 이외의 이벤트(MOVE 등)은 무시한다.
			sendCommand(prefix + "_" + postfix);
		}

		return false;
	}

	/*
	 * 스레드 목록
	 * 1. DroneServerConnector - [연결용 스레드] 소켓 서버와 연결을 시도한다.
	 * 2. DroneServerSender = [커맨드 전송 스래드] 소켓 서버에 커맨드를 전송한다.
	 */
	private class DroneServerConnector extends Thread {
		@Override
		public void run() {
			try {
				String host = etHost.getText().toString();
				String portStr = etPort.getText().toString();
				int port = Integer.parseInt(portStr);

				mSocket = new Socket(host, port);
				mSocketOut = new BufferedWriter(new OutputStreamWriter(mSocket.getOutputStream()));
				mSocketIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

			} catch (NumberFormatException ex) {
				Log.d(TAG, "포트번호가 정확하지 않음.", ex);
				Toast.makeText(MainActivity.this, "포트번호가 정확하지 않음.",
						Toast.LENGTH_SHORT).show();
			} catch (IOException ex) {
				Log.d(TAG, "컨넥션 실패", ex);
				Toast.makeText(MainActivity.this, "",
						Toast.LENGTH_SHORT).show();

				if (mSocketIn != null)
					try {mSocketIn.close();} catch (IOException e) { /* IGNORE */ }
				if (mSocketOut != null)
					try {mSocketOut.close();} catch (IOException e) { /* IGNORE */ }
				if (mSocket != null)
					try {mSocket.close();} catch (IOException e) { /* IGNORE */ }
			}
		}
	}

	private class DroneServerSender extends Thread {
		private String mSendMsg;

		DroneServerSender(String msg) {
			mSendMsg = msg;
		}

		@Override
		public void run() {
			if (mSocket != null && mSocket.isConnected()) {
				PrintWriter out = new PrintWriter(mSocketOut, true);
				out.println(mSendMsg);

				try {
					String line = mSocketIn.readLine();
					Log.d(TAG, "line: " + line);

					// TODO: 서버 응답에 대한 처리 추가 할 것. (응답이 없거나 OK가 아닌경우 재시도 등)

				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				Log.w(TAG, "Socket is not connected!");
				// TODO: 소켓연결이 끊긴경우에 대한 처리 추가 할 것. (재연결 등)
			}
		}
	}
}
