# Android socket server and client example

## 1. 범례(?)
  1. DS(Drone Simulator)
    - Module name : drone
  2. DRC(Drone Remote Controller)
    - Module name : controller
    
## 2. 실행방법
  1. DRC를 설치한 안드로이드 단말기의 핫스팟 활성
  2. DS를 설치한 안드로이드 단말기의 Wi-Fi를 DSR이 설치된 단말기에 접속
  3. DS를 실행하여 소켓 서버 IP 및 Port 확인
    - 앱 실행시 로그영역에 Listening...[192.168.xxx.xxx:9001] 의 형태로 기록 됨.
  4. DRC를 실행하여 IP와 포트번호 입력 후 Connect 버튼 선택
  5. DS에서 Receiving... 로그가 기록이 되면 연결 성공 상태임.
  6. DRC앱의 드론 조종 버튼을 선택하면, 선택한 버튼이 DS쪽에 표시 됨.
    - 소켓 통신 이용.

>>>
서버 클라이언트 간 명령 송수신 기능만 구현되어 있으며, 
예외사항(단절, 명령실행 실패 등)에 대한 구현은 되어 있지 않습니다.
>>>